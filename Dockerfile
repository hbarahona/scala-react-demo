FROM hseeberger/scala-sbt:8u312_1.5.7_3.1.0

RUN curl -sL https://deb.nodesource.com/setup_16.x | bash -

WORKDIR /app

RUN apt-get update && apt-get install -y \
    curl make wget git htop nano vim zsh \
    nodejs

EXPOSE 9000

#CMD [ "sbt", "run" ]
CMD [ "sh" ]

## Compilar docker
# docker build -t aodb-docker .

## Ejecutar docker
# docker run -it --rm -p 9000:9000 -v "$(pwd)":/app -v ~/.ivy2:/root/.ivy2  -v ~/.sbt:/root/.sbt  aodb-docker

## Ejecutar en la consola de la instancia docker
# sbt run 

#### SI NO SE EJECUTA DOCKER COMPOSE HACER LO SIGUIENTE: 

## Comentar la linea CMD [ "sbt", "run" ]


## Para OSX
# Dar permiso a Docker sobre las carpertas .ivy2 y . sbt
# En la barra de tareas en el icono de Docker ir a PReferences > Resource > File Sharing
# Agregar carpetas antes mencionadas, estas deben encontrarse dentro de la carpeta del usuario del pc
# Para visualizar en finder carpetas escondidas utilizar command+shift+.
