import {useEffect, useState} from 'react'
import logo from './logo.svg';
import './App.css';

function App() {
  const [estado, setEstado] = useState('')
  const [datos, setDatos] = useState([])

  useEffect(()=>{
    setEstado('cargando datos')
    fetch('http://localhost:9000/datos')
    .then( resp => resp.json())
    .then( data=> {
      setDatos(data)
      setEstado('datos obtenidos')
    })
    .catch( err => {
      console.log(err)
      setEstado('error')
    })
  },[])

  return (
    <div className="App">
      <header className="App-header">
        <img src={logo} className="App-logo" alt="logo" />
        <p>
          Edit <code>src/App.js</code> and save to reload.
        </p>
        <p>Estado: {estado}</p>
        <ul>
          {datos.map((dato, index) => <li key={index}>{dato.name}</li>)}
        </ul>
      </header>
    </div>
  );
}

export default App;
